


# Job SSIS Document

การใช้งาน Function Job Template For SSIS

```xml
<appSettings>
	<add key="ระบุ KEY" value="ระบุ VALUE" />
	...
</appSettings>
```
การใส่ key ของ Template มี 2 รูปแบบ
- แบบปกติ
```xml
key="{_program_index}_program_{function_index}_{function_name}"
```
- แบบที่ต้องระบุ parameter
```xml
key="{_program_index}_program_{function_index}_{function_name}_{parameter}"
```
> program_index = ลำดับของ program หรือ id ของ program โดยใส่เป็นตัวเลข [1 - n]
> function_index = ลำดับของ function ว่าจะทำ function ไหนก่อนหลังโดย
> function_name = ชื่อของ function ดังนี้ 
>  1. [start](#1-start)
>  2. [end](#2-end)
>  3. [check holiday](#3-check-holiday)
>  4. [execute text](#4-execute-text)
>  5. [execute file path](#5-execute-file-path)
>  6. [mail](#6-mail)
>  7. [create table and insert data](#7-create-table-and-insert-data)
>  8. [insert to](#8-insert-to)
>  9. [update to](#9-update-to)
>  10. [query to file](#10-query-to-file)
>  11. [copy](#11-copy)
>  
> parameter = ชื่อของ parameter ของ function 

## PROGRAM_RUN
```xml
<add key="PROGRAM_RUN" value="" />
<add key="PROGRAM_RUN" value="1" />
<add key="PROGRAM_RUN" value="1,4,5" />
```
> ระบุ value="" ในกรณีที่ให้รันทุกโปรแกรม หรือ ระบุ ลำดับ program_index ที่ต้องการรัน

## Parameter
```xml
${shortdate} = Date format yyyy-MM-dd
${longdate} = Date format yyyy-MM-dd HH:mm:ss.ffff
${basedir} = AppDomain.CurrentDomain.BaseDirectory (Path ของ Program)
```
### ตัวอย่าง
```xml
<add key="_1_program_2_executefilepath" value="10.251.20.40,MIS_UAT,username,password|${basedir}ScriptFiles\${shortdate}-test_exec.sql"/>
```

> คำอธิบายตัวอย่างคือ ทำการ Execute File 
>  - C:\Users\source\repos\NewMIS\MIS.Jobs\ScriptFiles\2019-09-01-test_exec.sql

# Function

## 1. START
```xml
<add key="_1_program_1_start" value="ชื่อของ program + function"/>
```
> function ***start*** จะต้องระบุ ***function_index*** เป็น 1 เสมอ

## 2. END
```xml
<add key="_1_program_999_end" value="ชื่อของ program + function"/>
```
> function ***end*** จะต้องระบุ ***function_index*** เป็น 999 เสมอหรือ เลขลำดับสุดท้าย

## 3. CHECK HOLDAY
```xml
<add key="_1_program_2_checkholiday" value=""/>
```
> ระบุ function ***checkholiday*** ในลำดับที่ 2 ในกรณีที่ function ต้องการตรวจสอบวันหยุด โดยวันหยุดจะไม่ทำ process ต่อไป
> - default @date=convert(varchar, getdate(), 120)
> - ในกรณีต้องการระบุวันที่ต้องการตรวจสอบใช้ `_1_program_param1` value ใช้ `@paramdate={sql date query}`
```xml
<add key="_1_program_param1" value="@paramdate=convert(varchar(10),getdate()-1,120)"/>
```


## 4. EXECUTE TEXT
```xml
<add key="_1_program_3_executetext" value="urldatabase,databasename,username,password|delete from table"/>
```
> ใช้สำหรับ execute query จาก query text หลัง `|`

## 5. EXECUTE FILE PATH
```xml
<add key="_1_program_3_executetext" value="urldatabase,databasename,username,password|${basedir}\ScriptFiles\test.sql"/>
```
> ใช้สำหรับ execute query จาก text file โดยอ่านจาก path file ที่ระบุไว้หลัง `|`

## 6. MAIL
```xml
<add key="_4_program_1_start" value="mail_${shortdate}"/>
    <add key="_4_program_2_mail_sender" value="MISJob.Dev@yuanta.co.th"/>
    <add key="_4_program_2_mail_to" value="Nopparat.M@yuanta.co.th"/>
    <add key="_4_program_2_mail_subject" value="ทดสอบส่งเมล By Job ${shortdate}"/>
    <add key="_4_program_2_mail_body" value="body ${shortdate}"/>
    <add key="_4_program_2_mail_attach" value="${basedir}logs\${shortdate}_LogFile.txt"/>
    <add key="_4_program_2_mail_execute" value="Y"/>
<add key="_4_program_999_end" value="mail_${shortdate}"/>
```
> function mail ต้องระบุ `function_index` เป็นเลขเดียวกันเพื่ออ้างอิง config ในการส่งเมล (ยังไม่สามารถระบุ body เมลได้) และต้องระบุ config `**`
> - key="_mail_sender" value="ผู้ส่ง" **
> - key="_mail_to" value="ผู้รับ โดยผู้รับหลายคนคั้นด้วย `;`" **
> - key="_mail_cc" value="CC โดย cc หลายคนคั้นด้วย `;`"
> - key="_mail_bcc" value="BCC โดย bcc หลายคนคั้นด้วย `;`"
> - key="_mail_attach" value="path file โดย file หลายไฟล์คั้นด้วย `;`" 
> - key="_mail_subject" value="ชื่อ subject" **
> - key="_mail_body" value="body mail" 
> - key="_mail_execute" value="Y or N" ** ใช้สำหรับส่งเมล 

## 7. CREATE TABLE AND INSERT DATA
```xml
<add key="_7_program_1_start" value="step_7_${shortdate}"/>
    <add key="_7_program_2_createtablefromselect_source" value="urldatabase,databasename,username,password|${basedir}ScriptFiles\query_select.sql"/>
    <add key="_7_program_8_createtablefromselect_desinationConnection" value="urldatabase,databasename,username,password"/>
    <add key="_7_program_8_createtablefromselect_desinationTableName" value="tablename"/>
    <add key="_7_program_8_createtablefromselect_execute" value="Y"/>
<add key="_7_program_999_end" value="step_7_${shortdate}"/>
```
> ใช้สำหรับ create table จากการ select ข้อมูลโดย column อิงจาก column select query
> `_insertto_desinationIsTemp` default = `N` ใส่ `Y` เมื่อต้องการ insert ลง table temp
> ตัวอย่าง source file query
```sql
SELECT val1, val2, val3 FROM _table_name_ 
```
> ตัวอย่าง desination file query
> 
```sql
INSERT  
  INTO _table_name_ (_column1_, _column2_, _column3_)  
VALUES ('{0}','{1}','{2}');
```

## 8. INSERT TO
```xml
<add key="_7_program_1_start" value="step_7_${shortdate}"/>
    <add key="_7_program_2_insertto_source" value="urldatabase,databasename,username,password|${basedir}ScriptFiles\query_select.sql"/>
    <add key="_7_program_8_insertto_desinationConnection" value="urldatabase,databasename,username,password"/>
    <add key="_7_program_8_insertto_desinationTableName" value="tablename"/>
    <add key="_7_program_8_insertto_execute" value="Y"/>
<add key="_7_program_999_end" value="step_7_${shortdate}"/>
```
> ใช้สำหรับ insert query จาก source file query ไปที่ desination file query โดยระบุตำแหน่ง query และตำแหน่ง insert ให้ตรงกัน
> `_insertto_desinationIsTemp` default = `N` ใส่ `Y` เมื่อต้องการ insert ลง table temp
> ตัวอย่าง source file query
```sql
SELECT val1, val2, val3 FROM _table_name_ 
```
> ตัวอย่าง desination file query
> 
```sql
INSERT  
  INTO _table_name_ (_column1_, _column2_, _column3_)  
VALUES ('{0}','{1}','{2}');
```

## 9. UPDATE TO
```xml
<add key="_8_program_1_start" value="step_8_${shortdate}"/>
    <add key="_8_program_2_updateto_source" value="urldatabase,databasename,username,password|${basedir}ScriptFiles\query_select.sql"/>
    <add key="_8_program_2_updateto_desination" value="urldatabase,databasename,username,password|${basedir}ScriptFiles\query_update.sql"/>
    <add key="_8_program_2_updateto_execute" value="Y"/>
<add key="_8_program_999_end" value="step_8_${shortdate}"/>
```
> ใช้สำหรับ update query จาก source file query ไปที่ desination file query โดยระบุตำแหน่ง query และตำแหน่ง update ให้ตรงกัน
>
> ตัวอย่าง source file query
```sql
SELECT val1, val2, val3 FROM _table_name_ 
```
> ตัวอย่าง desination file query
> 
```sql
UPDATE _table_name_  
   SET _column1_ = '{0}',
       _column2_ = '{1}',  
       _column3_ = '{2}'
 WHERE _condition_;
```

## 10. QUERY TO FILE
for ***querytype = "text"***
```xml
<add key="_5_program_1_start" value="querytofile_${shortdate}"/>
    <add key="_5_program_2_querytofile_connection" value="urldatabase,databasename,username,password"/>
    <add key="_5_program_2_querytofile_querytype" value="text"/>
    <add key="_5_program_2_querytofile_query" value="select * from [DateOfStartTask]"/>
    <add key="_5_program_2_querytofile_delimited" value=","/>
    <add key="_5_program_2_querytofile_header" value="Y"/>
    <add key="_5_program_2_querytofile_filepath" value="${basedir}logs\exports\${shortdate}_text.csv"/>
    <add key="_5_program_2_querytofile_execute" value="Y"/>
<add key="_5_program_999_end" value="querytofile_${shortdate}"/>
```
for ***querytype = "file"***
```xml
<add key="_5_program_1_start" value="querytofile_${shortdate}"/>
    <add key="_5_program_2_querytofile_connection" value="urldatabase,databasename,username,password"/>
    <add key="_5_program_2_querytofile_querytype" value="file"/>
    <add key="_5_program_2_querytofile_query" value="${basedir}ScriptFiles\query_select.sql"/>
    <add key="_5_program_2_querytofile_delimited" value=","/>
    <add key="_5_program_2_querytofile_header" value="Y"/>
    <add key="_5_program_2_querytofile_filepath" value="${basedir}logs\exports\${shortdate}_text.csv"/>
    <add key="_5_program_2_querytofile_execute" value="Y"/>
<add key="_5_program_999_end" value="querytofile_${shortdate}"/>
```
> function สำหรับแปลง query เป็นไฟล์
> - key="_querytofile_connection" value="connection ของ query" **
> - key="_querytofile_querytype" value="ประเภท query `text` or `file`" **
> - key="_querytofile_query" value="sql query หรือ path file query"
> - key="_querytofile_delimited" value="คั่นระหว่าง text ด้วยเครื่องหมาย"
> - key="_querytofile_header" value="`Y` or `N`"  สำหรับ write header file
> - key="_querytofile_filepath" value="path file ที่ต้องการ บันทึก" **
> - key="_querytofile_execute" value="`Y` or `N`" ** ใช้สำหรับ Execute function 

## 11. COPY
```xml
<add key="_6_program_1_start" value="copy_${shortdate}"/>
    <add key="_6_program_2_copy_domain" value=""/>
    <add key="_6_program_2_copy_username" value=""/>
    <add key="_6_program_2_copy_password" value=""/>
    <add key="_6_program_2_copy_source" value="C:\\Users\\source\\repos\\NewMIS\\MIS.Jobs\\bin\\Debug\\logs\\${shortdate}_LogFile.txt"/>
    <add key="_6_program_2_copy_destination" value="C:\\Users\\source\\repos\\NewMIS\\MIS.Jobs\\bin\\Debug\\logs\\${shortdate}_copy_LogFile.txt"/>
    <add key="_6_program_2_copy_execute" value="Y"/>
<add key="_6_program_999_end" value="copy_${shortdate}"/>
```
> ใช้สำหรับ copy ไฟล์
> - key="_copy_domain" value="domain ปลายทางที่ใช้ login"
> - key="_copy_username" value="username  ปลายทางที่ใช้ login"
> - key="_copy_password" value="password  ปลายทางที่ใช้ login"
> - key="_copy_source" value="path file ต้นทาง" **
> - key="_copy_destination" value="path file ปลายทาง" **
> - key="_copy_execute" value="`Y` or `N`" ** ใช้สำหรับ Execute function 
> ** จำเป็นต้องกรอก